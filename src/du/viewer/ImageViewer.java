package du.viewer;

import du.viewer.model.PictureFileTypes;
import du.viewer.view.ViewerController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class ImageViewer extends Application {
    private static File file;
    private ObservableList<File> imageFiles = FXCollections.observableArrayList();
    private Stage primaryStage;

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public File getFile() {
        return file;
    }

    public ObservableList<File> getImageFiles() {
        return imageFiles;
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            file = new File(args[0]);
        }
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        Scene scene = null;
        if (file != null) {
            loadImageFiles();
            scene = initViewerLayout();
        } else {
            file = new File(System.getProperty("user.home"));
            loadImageFiles();
            file = imageFiles.get(0);
            scene = intiBrowserLayout();
        }
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Scene initViewerLayout() {
        Scene scene = null;
        try {
            FXMLLoader loader = new FXMLLoader(ImageViewer.class.getResource("view/Viewer.fxml"));
            VBox viewerLayout = loader.load();
            scene = new Scene(viewerLayout);

            ViewerController viewerController = loader.getController();
            viewerController.setMainApp(this);

            viewerController.changeImage(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scene;
    }

    private Scene intiBrowserLayout() {
        Scene scene = null;
        //заглушка
        scene = initViewerLayout();
        //заглушка
        return scene;
    }

    public void loadImageFiles() {
        File parentDirectory = file.isDirectory()?file:file.getParentFile();
        for (File file : parentDirectory.listFiles()) {
            for (PictureFileTypes fileType : PictureFileTypes.values()) {
                if (file.getName().endsWith(fileType.toString()))
                    imageFiles.add(file);
            }
        }
    }
}
