package du.viewer.model;

public enum PictureFileTypes {
    PNG(".png"),
    JPG(".jpg"),
    JPEG(".jpeg"),
    BMP(".bmp");

    private String fileType;

    PictureFileTypes(String fileType) {
        this.fileType = fileType;
    }


    @Override
    public String toString() {
        return fileType;
    }
}
