package du.viewer.view;

import du.viewer.ImageViewer;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class ViewerController {
    private ImageViewer mainApp;
    private ObservableList<File> imageFiles;
    private File currentImageFile;
    private int currentFileIndex = -1;

    @FXML
    private Button next;

    @FXML
    private Button previous;

    @FXML
    private ImageView imageView;

    @FXML
    private void loadNextPicture() {
        if (currentFileIndex == -1)
            getCurrentFileIndex();
        currentFileIndex = currentFileIndex != imageFiles.size()-1 ? currentFileIndex + 1 : 0;
        changeImage(imageFiles.get(currentFileIndex));
    }

    @FXML
    private void loadPreviousPicture() {
        if (currentFileIndex == -1)
            getCurrentFileIndex();
        currentFileIndex = currentFileIndex != 0 ? currentFileIndex - 1 : imageFiles.size()-1;
        changeImage(imageFiles.get(currentFileIndex));
    }

    public void setMainApp(ImageViewer mainApp) {
        this.mainApp = mainApp;
        imageFiles = mainApp.getImageFiles();
        currentImageFile = mainApp.getFile();
        getCurrentFileIndex();
    }

    private void getCurrentFileIndex() {
        currentFileIndex = imageFiles.indexOf(currentImageFile);
    }

    public void changeImage(File file) {
        Image image = new Image(file.toURI().toString());
        imageView.setImage(image);
        mainApp.getPrimaryStage().setTitle("ImageViewer - " + file.getName());
    }
}
